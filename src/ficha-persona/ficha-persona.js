import { LitElement, html} from 'lit-element' ;

class FichaPersona extends LitElement{
    
    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object}
        };
    }

    render(){
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany"></input>
                <br />
            </div>
        `;
    }
}

customElements.define('ficha-persona', FichaPersona);